# Tiny emulators CPC

This is an adaptation of [Tiny Emulators](https://github.com/floooh/chips-test/), specifically the
webassembly version of Amstrad CPC emulator.

The code was modified to just load a "game.sna" found in the same folder as the
emulator. This allows to easily embed Amstrad CPC games on iframes, for example on itch.io.

## Build

You will need [emscripten](https://emscripten.org) installed on your machine.

Create a output directory if you don't want to mix the output files with the sources.

```
mkdir -p out
```

Compile with:

```
emcc \
   --shell-file shell.html \
   -Icommon -Ichips -Isokol -Isokol/util -Iroms \
   -DSOKOL_GLES2 \
   -DNDEBUG \
   -O3 -flto \
   cpc.c common/common.c common/sokol.c -o out/index.html
```

On the output folder you will have this 3 files: index.html, index.js and
index.wasm; you will need to add your game.sna to the same folder and serve it
over http.

