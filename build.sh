#!/bin/sh

set -x

INCLUDES="-Icommon -Ichips -Isokol -Isokol/util -Iroms"
DEFINES="-DSOKOL_GLES2"

mkdir -p out

shaders() {
  sokol-shdc -i common/shaders.glsl -l glsl330:glsl100:glsl300es:hlsl4 -o common/shaders.glsl.h
}

linux() {
 LIBS="-lm -lX11 -lGLESv2 -lEGL -lXcursor -lXi -lasound"
 time clang $INCLUDES $LIBS -DSOKOL_GLES2 -g cpc.c common/common.c common/sokol.c -o out/cpc
}

web() {
 time emcc --shell-file shell.html $INCLUDES -DSOKOL_GLES2 -DNDEBUG -O3 -flto cpc.c common/common.c common/sokol.c -o out/index.html
}

${1-linux}


